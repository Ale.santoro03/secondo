//  main.c
//  craps
//
//  Created by Alessandro Santoro on 23/11/22.
//  VEDERE CONFRONTO CON IL PDF PAGINA 183

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

int main() {
    
    bool continua=false, won=false, lost=false;
    int old_punt;
    
    srand(time(NULL));
    int sum=(1+rand()%6)+(1+rand()%6);
    printf("\nla somma è: %d", sum);
    
    if(sum==7 || sum==11){
        won=true;
        printf("\nil giocatore ha vinto");
    }else if(sum==2 || sum==3 || sum==12){
        lost=true;
        printf("\nil giocatore ha perso");
    }else{
        continua=true;
        old_punt=sum;
    }
    
    while(continua==true){
        sum=(1+rand()%6)+(1+rand()%6);
        printf("\nla somma è: %d", sum);
        
        if(sum==7){
            lost=true;
            printf("\n il giocatore ha perso");
            continua=false;
        }else if(sum==old_punt){
            won=true;
            printf("\n il giocatore ha vinto");
            continua=false;
        }
    }
    
    /*
    //VEDERE QUANTI 5 VENGONO INSERITI DALL'UTENTE SU 10 VOLTE
    int numero, uscito=0;
    int contatore=0;
    
    while(contatore<10){
        cin>>numero;
        if(numero==5){
            uscito=uscito+1; //la variabile "uscito" viene incrementata ogni volta che inserisci 5, in modo tale che alla fine ti dice quante volte l'hai inserito
        }
        contatore=contatore+1;
    }
    cout<<"il numero è uscito"<<uscito<<"volte";
    */
    
    return 0;
}


//ciao 